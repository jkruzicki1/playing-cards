// Jacob Kruzicki

#include <iostream>
#include <conio.h>

enum Rank
{
    Two = 2,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace
};

enum Suit
{
    Club,
    Spade,
    Heart,
    Diamond
};

struct Card
{
    Suit suit;
    Rank rank;
};

int main()
{
    (void)_getch();
    return 0;
}
